package svg

import org.junit.Test

import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 13/07/13
 * Time: 8:44 AM
 */
class SvgPathBuilderTest {
    @Test
    void testToSvg() {
        SvgPathBuilder builder = new SvgPathBuilder()
        CubicBezierPoints bezier = new CubicBezierPoints(new Point(1, 2), new Point(3, 4), new Point(5, 6), new Point(7, 8))
        builder.startPath().startDraw().relative()
        .moveTo(bezier.start)
        .cubicBezier(bezier)
        .endDraw()
        .style("stroke: black;")
        .endPath()
        assertEquals('<path d=" m1,2 c3,4 5,6 7,8" style="stroke: black;"/>', builder.toString())
    }
}
