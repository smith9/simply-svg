package svg

import org.junit.Test

/**
 * User: david
 * Date: 12/07/13
 * Time: 11:33 PM
 */
class PointTest {
    @Test
    void testScale() {
        TestUtil.checkPoint(new Point(10, 20), new Point(1,2).scale(10, 10))
        TestUtil.checkPoint(new Point(-10, -8), new Point(1,2).scale(-10, -4))
    }


    @Test
    void testTranslate() {
        TestUtil.checkPoint(new Point(6, 5), new Point(1,2).translate(new Point(5,3)))
        TestUtil.checkPoint(new Point(-4, -1), new Point(1,2).translate(new Point(-5,-3)))
    }

    @Test
    void testRotate() {
        TestUtil.checkPoint(new Point(0, -2), new Point(2,0).rotate(90))
        TestUtil.checkPoint(new Point(0, 2), new Point(2,0).rotate(-90))
        TestUtil.checkPoint(new Point(1, -2), new Point(2, 1).rotate(90))

    }
//    @Test
//    void testRotate() {
//        TestUtil.checkPoint(new Point(0, 2), new Point(2,0).rotate(90))
//        TestUtil.checkPoint(new Point(0, 2), new Point(-2,0).rotate(-90))
//        TestUtil.checkPoint(new Point(-1, 2), new Point(2,1).rotate(90))
//
//    }

    @Test
    void testMirrorOnXAxis() {
        TestUtil.checkPoint(new Point(1,-2), new Point(1,2).mirrorOnXAxis())
        TestUtil.checkPoint(new Point(-1,2), new Point(-1,-2).mirrorOnXAxis())

    }
}
