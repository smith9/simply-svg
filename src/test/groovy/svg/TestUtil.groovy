package svg

import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 12/07/13
 * Time: 11:35 PM
 */
class TestUtil {
    public static void checkPoint(Point expected, Point actual) {
        checkPoint("", expected, actual)
    }
    public static void checkPoint(String messagePrefix, Point expected, Point actual) {
        String message = "expected (${expected.x},${expected.y}) actual (${actual.x},${actual.y})"
        if(messagePrefix) {
            message = messagePrefix + " " + message
        }
        assertEquals("check x $message", expected.x, actual.x, 0.00001)
        assertEquals("check y $message", expected.y, actual.y, 0.00001)
    }
}
