package svg

import org.junit.Test

/**
 * User: david
 * Date: 12/07/13
 * Time: 8:37 PM
 */
class BezierCalculatorTest {
    @Test
    void testCalculate() {
        check(50, 90, new Point(35.3553390593, 35.3553390593), new Point(54.8815536469, 15.8291244718))
        check(10, 45, new Point(9.2387953251, 3.8268343237), new Point(10.2537348916, 1.3765534572))
    }

    void check(int radius, int angle, Point expectedStart, Point expectedControlStart) {
        CubicBezierPoints calculated = new BezierCircularArcCalculator().calculate(radius, angle)
        TestUtil.checkPoint("start", expectedStart, calculated.start)
        TestUtil.checkPoint("control start", expectedControlStart, calculated.points[1])
        TestUtil.checkPoint("control end", new Point(expectedControlStart.x, expectedControlStart.y.negate()), calculated.points[2])
        TestUtil.checkPoint("end", new Point(expectedStart.x, expectedStart.y.negate()), calculated.points[3])
    }

}
