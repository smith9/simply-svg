package svg

/**
 * User: david
 * Date: 12/07/13
 * Time: 5:20 PM
 */
class BezierCircularArcCalculator {
    /**
     * calculates the points required for a bezier curve with the points being symetrical about the x-axis
     * @param radius of the circular arc
     * @param angle of the arc, must be less than 90 deg
     * @return
     */
    CubicBezierPoints calculate(BigDecimal radius, BigDecimal angle) {
        if (angle > 90) {
            return calculateAngleGreaterThanTo90(angle, radius)
        }
        return calculateAngleLessThanEqualTo90(angle, radius)
    }

    CubicBezierPoints calculateAngleGreaterThanTo90(BigDecimal angle, BigDecimal radius) {
        int numRequiredArcs = calculateNumRequiredArcs(angle)
        BigDecimal partArcAngle = angle / numRequiredArcs
        CubicBezierPoints partArcPoints = calculateAngleLessThanEqualTo90(partArcAngle, radius).rotate(partArcAngle/2) as CubicBezierPoints
        CubicBezierPoints completeArc = new CubicBezierPoints(partArcPoints.points)
        (2..numRequiredArcs).each { int index ->
            completeArc.addPoints(partArcPoints.rotate((index-1) * partArcAngle) as CubicBezierPoints)
        }
        completeArc = completeArc.rotate(-angle / 2) as CubicBezierPoints
        return completeArc;
    }

    int calculateNumRequiredArcs(BigDecimal angle) {
        BigDecimal[] divAndRemainder = angle.divideAndRemainder(90)
        int div = divAndRemainder[0]
        if(divAndRemainder[1] > BigDecimal.ZERO) {
            div++
        }
        return div
    }

    private CubicBezierPoints calculateAngleLessThanEqualTo90(BigDecimal angle, BigDecimal radius) {
        Point start = new Point(calcXpositionOnCircle(angle), calcYpositionOnCircle(angle))
        Point end = mirrorOnXAxis(start)
        Point controlStart = calcControlStart(start)
        Point controlEnd = mirrorOnXAxis(controlStart)
        return new CubicBezierPoints(start.scale(radius, radius),
                controlStart.scale(radius, radius),
                controlEnd.scale(radius, radius),
                end.scale(radius, radius))
    }

    Point mirrorOnXAxis(Point point) {
        return point.mirrorOnXAxis()
    }

    Point calcControlStart(Point start) {
        BigDecimal x = (4 - start.x) / 3
        BigDecimal y = ((1 - start.x) * (3 - start.x)) / (3 * start.y)
        return new Point(x, y)
    }

    private BigDecimal calcXpositionOnCircle(BigDecimal angle) {
        return Math.cos(Math.toRadians(angle / 2))
    }

    private BigDecimal calcYpositionOnCircle(BigDecimal angle) {
        return Math.sin(Math.toRadians(angle / 2))
    }
}
