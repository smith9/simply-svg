package svg

import svg.ends.JigsawEnd

/**
 * User: david
 * Date: 13/07/13
 * Time: 8:57 AM
 */
class SvgPathBuilder {

    SvgPathBuilder lineTo(Point toPoint) {
        println "line to"
        controlLetter('L' as char)
        point(toPoint)
        return this;
    }

    SvgPathBuilder newLine() {
        builder.append("\n")
        return this;
    }

    SvgPathBuilder(Transform coordinateTransform) {
        this.coordinateTransform = coordinateTransform
    }

    SvgPathBuilder() {
    }

    StringBuilder builder = new StringBuilder()
    boolean relative = true
    boolean controlPending = false
    Transform coordinateTransform

    public SvgPathBuilder startPath() {
        builder.append("<path")
        return this
    }

    public SvgPathBuilder endPath() {
        builder.append("/>")
        return this
    }

    public SvgPathBuilder startDraw() {
        builder.append(' d="')
        return this
    }

    public SvgPathBuilder endDraw() {
        builder.append('"')
        return this
    }

    public SvgPathBuilder moveTo(Point movePoint) {
        println "move to"
        controlLetter('m' as char)
        point(movePoint)
        return this
    }

    private SvgPathBuilder controlLetter(char controlLetter) {
        char controlRelativeOrAbsolute = relative ? controlLetter.toLowerCase() : controlLetter.toUpperCase()
        println "control = $controlRelativeOrAbsolute"
        builder.append(" ").append(controlRelativeOrAbsolute)
        controlPending = true
        return this
    }

    public SvgPathBuilder relative() {
        relative = true
        return this
    }

    public SvgPathBuilder absolute() {
        relative = false
        return this
    }

    @Override
    public String toString() {
        return builder.toString();
    }

    public SvgPathBuilder cubicBezier(CubicBezierPoints points) {
        println "drawing points for cubic bezier"
        controlLetter('c' as char)
        points.points.tail().each { Point curvePoint ->
            point(curvePoint)
        }
        return this;
    }

    private SvgPathBuilder point(Point point) {
        Point svgPoint = toSvg(point)
        avoidSpaceAfterControl()
        BigDecimal x = svgPoint.x
        BigDecimal y = svgPoint.y
        println("point: x=${x.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString()}, " +
                "y=${y.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString()}")
//        x = x.setScale(2, BigDecimal.ROUND_HALF_UP)
//        y = y.setScale(2, BigDecimal.ROUND_HALF_UP)
        builder.append(x.toPlainString()).append(",").append(y.toPlainString())
        return this
    }

    Point toSvg(Point point) {
        if (coordinateTransform) {
            return coordinateTransform.transform(point)
        } else {
            return point
        }
    }

    private void avoidSpaceAfterControl() {
        if (!controlPending) {
            builder.append(" ")
        }
        controlPending = false
    }

    SvgPathBuilder style(String style) {
        builder.append(' style="').append(style).append('"')
        return this;
    }

    SvgPathBuilder xmlDeclaration() {
        builder.append('<?xml version="1.0" encoding="UTF-8" standalone="no"?>')
        return this
    }

    SvgPathBuilder startSvg(int width, int height) {
        builder.append("""<svg width="${width}px" height="${height}px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">""")
        return this
    }

    SvgPathBuilder startGroup(Map<String, String> attributes) {
        builder.append("<g")
        attributes.each { String key, String value ->
            builder.append(" ").append(key).append('="').append(value).append('"')
        }
        builder.append(">")
        return this
    }
    SvgPathBuilder endSvg() {
        end("svg")
    }
    SvgPathBuilder endGroup() {
        end("g")
    }
    private SvgPathBuilder end(String elementName) {
        builder.append("</$elementName>")
        return this
    }
}
