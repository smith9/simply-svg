package svg

/**
 * User: david
 * Date: 13/07/13
 * Time: 10:02 PM
 */
interface Transform {
    Point transform(Point point)
}
