package svg.ends

import svg.Point
import svg.SvgPathBuilder

/**
 * User: david
 * Date: 17/07/13
 * Time: 10:10 PM
 */
class LineEnd {
    List<Point> points = []

    public SvgPathBuilder build(SvgPathBuilder builder, BigDecimal angle, Point transform, boolean reversePaths) {
        println "linear end builder"
        int numPoints = points.size()
        IntRange skipFirstPointReverse = (numPoints - 2)..0
        IntRange skipFirstPointForward = 1..(numPoints - 1)
        IntRange indices = reversePaths ? skipFirstPointReverse : skipFirstPointForward

        indices.each { int index ->
            Point currentPoint = points[index]

            currentPoint = currentPoint.translate(transform)
            if(angle != 0) {
                println "rotating $angle"
                currentPoint = currentPoint.rotate(angle)
            }
            builder.lineTo(currentPoint)

        }
        return builder
    }

}
