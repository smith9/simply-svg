package svg.ends

import svg.Point
import svg.SvgPathBuilder

/**
 * User: david
 * Date: 14/07/13
 * Time: 9:00 PM
 */
class ArrowEnd implements EndBuilder{
    BigDecimal endLength
    BigDecimal heightRatio = 3.5
    BigDecimal tailWidthRatio = 3
    BigDecimal tailHeightRatio = 2.5
    BigDecimal headWidthRatio = 2.5
    LineEnd end = new LineEnd()

    ArrowEnd() {
    }
    void init(BigDecimal endWidth, boolean clockwise) {
        this.endLength = endWidth
        BigDecimal arrowHeight = endLength / heightRatio
        BigDecimal tailHeight = arrowHeight / tailHeightRatio
        BigDecimal headWidth = endLength / headWidthRatio
        BigDecimal tailWidth = headWidth / tailWidthRatio
        BigDecimal flatWidth = (endLength - tailWidth)/2
        println "arrow height=$arrowHeight, tail height=$tailHeight, head width=$headWidth, tail width=$tailWidth, flat width=$flatWidth"
        BigDecimal midPoint = endLength/2
        end.points << new Point(0,0)
        int direction = clockwise ? 1 : -1
        end.points << new Point(flatWidth, 0)
        end.points << new Point(flatWidth, tailHeight * direction)
        end.points << new Point(flatWidth - (headWidth - tailWidth)/2, tailHeight * direction)
        end.points << new Point(midPoint, arrowHeight * direction)
        mirror(midPoint)
        println "points $end.points"
    }

    private void mirror(BigDecimal midPoint) {
        (3..0).each {
            Point currentPoint = end.points[it]
            BigDecimal mirroredOnMidpoint = 2 * (midPoint - currentPoint.x)
            Point mirroredPoint = currentPoint.translate(new Point(mirroredOnMidpoint, 0))
            end.points << mirroredPoint
        }

    }

    @Override
    public String toString() {
        return "points = ${end.points}"
    }

    public SvgPathBuilder build(SvgPathBuilder builder, BigDecimal angle, Point transform, boolean reversePaths) {
        println "arrow end"
        end.build(builder, angle, transform, reversePaths)
        return builder
    }

}
