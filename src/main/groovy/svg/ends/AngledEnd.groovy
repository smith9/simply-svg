package svg.ends

import svg.Point
import svg.SvgPathBuilder

/**
 * User: david
 * Date: 14/07/13
 * Time: 9:00 PM
 */
class AngledEnd implements EndBuilder{
    BigDecimal endLength
    BigDecimal heightRatio = 4
    LineEnd end = new LineEnd()

    AngledEnd() {
    }
    void init(BigDecimal endWidth, boolean clockwise) {
        this.endLength = endWidth
        end.points << new Point(0,0)
        int direction = clockwise ? 1 : -1
        end.points << new Point(endLength/2, endLength/heightRatio * direction)
        end.points << new Point(endLength, 0)
    }



    @Override
    public String toString() {
        return "points = ${end.points}"
    }

    public SvgPathBuilder build(SvgPathBuilder builder, BigDecimal angle, Point transform, boolean reversePaths) {
        println "Angled End"
        end.build(builder, angle, transform, reversePaths)
        return builder
    }

}
