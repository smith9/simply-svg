package svg.ends

import svg.CubicBezierPoints
import svg.Point
import svg.SvgPathBuilder

/**
 * User: david
 * Date: 14/07/13
 * Time: 9:00 PM
 */
class FlatEnd implements EndBuilder {
    BigDecimal endLength
    LineEnd end = new LineEnd()

    FlatEnd() {
    }

    void init(BigDecimal endWidth, boolean clockwise) {
        this.endLength = endWidth
        end.points << new Point(0, 0)
        end.points << new Point(endLength, 0)
    }

    @Override
    public String toString() {
        return "points = ${end.points}"
    }

    public SvgPathBuilder build(SvgPathBuilder builder, BigDecimal angle, Point transform, boolean reversePaths) {
        println "flat end"
        end.build(builder, angle, transform, reversePaths)
        return builder
    }

}
