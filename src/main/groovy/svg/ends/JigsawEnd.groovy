package svg.ends

import svg.CubicBezierPoints
import svg.Point
import svg.SvgPathBuilder

/**
 * User: david
 * Date: 14/07/13
 * Time: 9:00 PM
 */
class JigsawEnd implements EndBuilder{
    BigDecimal endLength
    BigDecimal control1Angle = 45
    BigDecimal control23Angle = 45
    BigDecimal control1Ratio = 15
    BigDecimal control2Ratio = 15
    BigDecimal control3Ratio = 13
    BigDecimal control4Ratio = 3
    BigDecimal heightRatio = 3
    BigDecimal horizontalRatio = 2.5

    List<CubicBezierPoints> curves = []

    JigsawEnd() {
    }
    void init(BigDecimal endWidth, boolean clockwise) {
        this.endLength = endWidth
        BigDecimal flatLength = endLength / horizontalRatio
        BigDecimal control1Length = endLength / control1Ratio
        BigDecimal control1X = calculateX(control1Angle, control1Length)
        BigDecimal control1Y = calculateY(control1Angle, control1Length)
        BigDecimal control2Length = endLength / control2Ratio
        BigDecimal control2X = calculateX(control23Angle, control2Length)
        BigDecimal control2Y = calculateY(control23Angle, control2Length)
        curves << new CubicBezierPoints(new Point(0, 0),
        new Point(control1X, control1Y),
        new Point(flatLength - control2X, control2Y),
        new Point(flatLength, 0))

        BigDecimal control3Length = endLength / control3Ratio
        BigDecimal control3X = calculateX(control23Angle, control3Length)
        BigDecimal control3Y = calculateY(control23Angle, control3Length)
        BigDecimal control4Length = endLength / control4Ratio
        BigDecimal height = endLength / heightRatio
        double midPointX = endLength / 2
        curves << new CubicBezierPoints(new Point(flatLength, 0),
        new Point(flatLength + control3X, 0 - control3Y),
        new Point(midPointX - control4Length, height),
        new Point(midPointX, height))

        curves << curves[1].mirrorOnX(midPointX)
        curves << curves[0].mirrorOnX(midPointX)

        if(!clockwise) {
            mirrorOnY()
        }
    }

    void mirrorOnY() {
        println "drawing ends anticlockwise"
        (0..3).each { int index ->
            curves[index] = curves[index].mirrorOnY(0)
        }

    }

    private double calculateX(BigDecimal angle, BigDecimal length) {
        return Math.cos(Math.toRadians(angle)) * length
    }
    private double calculateY(BigDecimal angle, BigDecimal length) {
        return Math.sin(Math.toRadians(angle)) * length
    }

    @Override
    public String toString() {
        return curves.join("\n")
    }

    public SvgPathBuilder build(SvgPathBuilder builder, BigDecimal angle, Point transform, boolean reversePaths) {
        println "jigsaw end"
        IntRange indices = reversePaths ? 3..0 : 0..3
        indices.each { int index ->
            println "processing end part $index"
            CubicBezierPoints points = curves[index]
            if(reversePaths) {
                println "reversing paths"
                points = points.reversePath()
            }
            points = points.translate(transform) as CubicBezierPoints
            if(angle != 0) {
                println "rotating $angle"
                points = points.rotate(angle)
            }
            builder.cubicBezier(points)
        }
        return builder
    }

}
