package svg.ends

import svg.Point
import svg.SvgPathBuilder

/**
 * User: david
 * Date: 15/07/13
 * Time: 11:39 PM
 */
public interface EndBuilder {
    public void init(BigDecimal endWidth, boolean clockwise)
    public SvgPathBuilder build(SvgPathBuilder builder, BigDecimal angle, Point transform, boolean reversePaths)
}