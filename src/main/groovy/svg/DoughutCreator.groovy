package svg

import svg.ends.AngledEnd
import svg.ends.ArrowEnd
import svg.ends.EndBuilder
import svg.ends.FlatEnd
import svg.ends.JigsawEnd

/**
 * User: david
 * Date: 15/07/13
 * Time: 9:46 PM
 */
class DoughutCreator {
    public static void main(String[] args) {
        List stdAnglesNoGaps = [[180, 0], [120, 0], [90, 0], [72, 0], [60, 0], [51.42857142857143, 0], [45, 0]]
        createMultipleDoughnuts(ArrowEnd.class,
                stdAnglesNoGaps + [[162, 18], [102, 18], [72, 18], [54, 18], [42, 18]],
                stdAnglesNoGaps + [[168, 12], [108, 12], [78, 12], [60, 12], [48, 12]])
        createMultipleDoughnuts(FlatEnd.class,
                        stdAnglesNoGaps + [[172, 8], [112, 8], [82, 8], [64, 8], [52, 8]],
                        stdAnglesNoGaps + [[175, 5], [115, 5], [85, 5], [67, 5], [55, 5]])
        createMultipleDoughnuts(AngledEnd.class,
                        stdAnglesNoGaps + [[172, 8], [112, 8], [82, 8], [64, 8], [52, 8]],
                        stdAnglesNoGaps + [[175, 5], [115, 5], [85, 5], [67, 5], [55, 5]])
        createMultipleDoughnuts(JigsawEnd.class,
                        stdAnglesNoGaps + [[158, 22], [98, 22], [68, 22], [50, 22], [38, 22]],
                        stdAnglesNoGaps + [[166, 14], [106, 14], [76, 14], [58, 14], [46, 14]])
//        createSingleDoughnut()
    }

    private static void createSingleDoughnut() {
        DoughnutSettings settings = new DoughnutSettings()
        settings.angle = 350
        settings.angleGap = 0
        settings.outerRadius = 60
        settings.innerRadius = 15
        settings.endBuilder = new FlatEnd()
        settings.endsClockwise = true

        DoughutCreator doughutCreator = new DoughutCreator()
        doughutCreator.createSingle(settings)
    }

    static void createMultipleDoughnuts(Class endBuilder, List smallInnerAngles, List largeInnerAngles) {
        createMultipleDoughnuts(
                "inner-small",
                endBuilder,
                smallInnerAngles ,
                [[15, 50]])
        createMultipleDoughnuts(
                "inner-large",
                endBuilder,
                largeInnerAngles,
                [[25, 50]])
    }

    static void createMultipleDoughnuts(String filenamePrefix, Class endBuilder, List angles, List radii) {
        List<DoughnutSettings> settings = []
        angles.each { List<BigDecimal> angleAndGap ->
            radii.each { List<Integer> innerOuterRadius ->
                [true, false].each { boolean clockwise ->
                    DoughnutSettings setting = new DoughnutSettings()
                    setting.angle = angleAndGap[0]
                    setting.angleGap = angleAndGap[1]
                    setting.outerRadius = innerOuterRadius[1]
                    setting.innerRadius = innerOuterRadius[0]
                    setting.endBuilder = endBuilder.newInstance() as EndBuilder
                    setting.endsClockwise = clockwise
                    settings << setting
                }
            }
        }
        DoughutCreator doughutCreator = new DoughutCreator()
        doughutCreator.createMultiple(settings, 6, filenamePrefix)
    }

//    static final List<String> COLOURS = ["#990066","#990099","#663399","#333399","#0066FF","#6699FF","#3366FF", "#003399"]
    static final List<String> COLOURS = ["red", "green", "blue", "darkgray", "gold", "pink", "purple", "brown"]
    SvgPathBuilder builder = new SvgPathBuilder()
    DoughnutSettings opt

    public void createSingle(DoughnutSettings opt) {
        this.opt = opt
        int square = opt.outerRadius * 2 + 2
        builder.xmlDeclaration()
        builder.startSvg(square, square)
        builder.startGroup(style: "fill:none; stroke-linejoin: round;", transform: "translate(${opt.outerRadius},${opt.outerRadius})")

        createPaths()
        builder.endGroup().endSvg()
        String direction = opt.endsClockwise ? "clockwise" : "anticlockwise"
        String name = "segment-${opt.endBuilder.class.name.replaceFirst(/.*\./, "")}-$direction"
        String filename = "/Users/david/temp/${name}.svg"
        println "file=${filename}"
        new File(filename).text = builder.toString().replaceAll("C", "\nC").replaceAll(" ", "\n")
        println builder.toString()
    }

    public void createMultiple(List<DoughnutSettings> opts, int numColumns, String filenamePrefix) {
        int numRows = new BigDecimal(opts.size() / numColumns).setScale(0, BigDecimal.ROUND_UP)
        BigDecimal maxRadius = opts.max { DoughnutSettings opt ->
            return opt.outerRadius
        }.outerRadius
//        int width = maxRadius * 2 * numColumns + 3
//        int height = maxRadius * 2 * numRows + 3
        int width = 1000
        int height = 1000
        builder.xmlDeclaration()
        builder.startSvg(width, height)

        int optIndex = 0

        (0..numRows - 1).each { int row ->
            (0..numColumns - 1).each { int column ->
                if (optIndex < opts.size()) {
                    println "processing option index=$optIndex row=$row column=$column"
                    this.opt = opts[optIndex]
                    // shape is at origin so need to move it half a radius
                    // and a whole diameter for each row/column
                    // and 1 pixel for each doughnut
                    BigDecimal translateX = maxRadius + column * maxRadius * 2 + column + 1
                    BigDecimal translateY = maxRadius + row * maxRadius * 2 + row + 1
                    println "translate $translateX, $translateY"
                    builder.startGroup(style: "fill:none; stroke-linejoin: round;", transform: "translate(${translateX},${translateY})")
                    createPaths()
                    builder.endGroup()
                }
                optIndex++
            }
        }

        builder.endSvg()
        String name = "${filenamePrefix}-${opt.endBuilder.class.name.replaceFirst(/.*\./, "")}"
        new File("/Users/david/temp/${name}.svg").text = builder.toString()//.replaceAll("C", "\nC").replaceAll(" ", "\n")
        println builder.toString()
    }

    private SvgPathBuilder createPaths() {
        println "creating paths for options $opt"
        BezierCircularArcCalculator bezier = new BezierCircularArcCalculator()
        CubicBezierPoints unitCircularArc = bezier.calculate(1, opt.angle)

        int numSegments = 360 / (opt.angle + opt.angleGap)
//        numSegments = 2
        opt.endBuilder.init(opt.outerRadius - opt.innerRadius, opt.endsClockwise)
        numSegments.times { int index ->
            drawSegment(builder, index, unitCircularArc)
        }
        return builder
    }

    private void drawSegment(SvgPathBuilder builder, int index, CubicBezierPoints unitCircularArc) {

        println "${"="*60}\n segment $index"
//            println "jigsaw = $endBuilder"
        int rotation = -opt.angle / 2 - index * (opt.angle + opt.angleGap)
        BigDecimal startEndAngle = index * (-opt.angle - opt.angleGap)
        BigDecimal endEndAngle = (index + 1) * (-opt.angle - opt.angleGap) + opt.angleGap
        println "rotation of index $index = $rotation"
        CubicBezierPoints radiusInnerRotated = unitCircularArc.rotate(rotation).scale(opt.innerRadius, opt.innerRadius) as CubicBezierPoints
        radiusInnerRotated = radiusInnerRotated.reversePath()
        CubicBezierPoints radiusOuterRotated = unitCircularArc.rotate(rotation).scale(opt.outerRadius, opt.outerRadius) as CubicBezierPoints
        builder.startPath().startDraw().absolute()
                .moveTo(radiusOuterRotated.start)
        println "drawing outer radius"
        builder.cubicBezier(radiusOuterRotated)
        println "end builder - start"
        opt.endBuilder.build(builder, startEndAngle, new Point(opt.innerRadius, 0), true)
        println "drawing inner radius"
        builder.cubicBezier(radiusInnerRotated)
        println "end builder - end"
        opt.endBuilder.build(builder, endEndAngle, new Point(opt.innerRadius, 0), false)
        builder.endDraw()
                .style("stroke: black; fill: ${opt.colours[index]}")
                .endPath()
                .newLine()
    }

}
