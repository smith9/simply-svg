package svg

/**
 * User: david
 * Date: 12/07/13
 * Time: 5:21 PM
 */
public class Point {
    private BigDecimal x
    private BigDecimal y

    Point(BigDecimal x, BigDecimal y) {
        this.x = x
        this.y = y
    }

    /**
     * e.g. scaling point (1,2) by (10,10) = (10,20)
     * @param xScale, amount to scale in x directions, -1 to mirror on y-axis
     * @param yScale, amount to scale in y directions, -1 to mirror on x-axis
     * @return a new Point scaled in both x and y directions
     */
    public Point scale(BigDecimal xScale, BigDecimal yScale) {
        return new Point(x * xScale, y * yScale)
    }

    public Point translate(Point translate) {
        return new Point(x + translate.x, y + translate.y)
    }

    /**
     *
     * @param angle to rotate by in degrees
     * @return
     */
    public Point rotate(BigDecimal angle, boolean clockwise = false) {
        double radians = Math.toRadians(angle)
//        if(clockwise) {
            return calculateRotationClockwise(radians)
//        } else {
//            return calculateRotationAntiClockwise(radians)
//        }
    }

    private Point calculateRotationClockwise(double radians) {
        double rotatedX = x * Math.cos(radians) + y * Math.sin(radians)
        double rotatedY = y * Math.cos(radians) - x * Math.sin(radians)
        return new Point(rotatedX, rotatedY)
    }
//    private Point calculateRotationAntiClockwise(double radians) {
//        double rotatedX = x * Math.cos(radians) - y * Math.sin(radians)
//        double rotatedY = y * Math.cos(radians) + x * Math.sin(radians)
//        return new Point(rotatedX, rotatedY)
//    }

    public Point mirrorOnXAxis() {
        return new Point(x, y.negate())
    }



    @Override
    public java.lang.String toString() {
        final java.lang.StringBuilder sb = new java.lang.StringBuilder("Point{");
        sb.append("x=").append(x.toPlainString());
        sb.append(", y=").append(y.toPlainString());
        sb.append('}\n');
        return sb.toString();
    }
}
