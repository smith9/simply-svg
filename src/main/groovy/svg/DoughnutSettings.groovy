package svg

import svg.ends.EndBuilder
import svg.ends.JigsawEnd

/**
 * User: david
 * Date: 17/07/13
 * Time: 12:39 PM
 */
class DoughnutSettings {
    static final List<String> COLOURS = ["#66CCFC","#B95CE6","#FA6FCF","#FDCC66","#CCFF66","#66FFCC","#66FFFF", "#FFFF33"]
    BigDecimal angle = 90
    BigDecimal angleGap = 0
    BigDecimal outerRadius = 50
    BigDecimal innerRadius = 25
    boolean endsClockwise = false
    List<String> colours = COLOURS
    EndBuilder endBuilder = new JigsawEnd()

    @Override
    public java.lang.String toString() {
        final java.lang.StringBuilder sb = new java.lang.StringBuilder("DoughnutSettings{");
        sb.append("angle=").append(angle);
        sb.append(", angleGap=").append(angleGap);
        sb.append(", outerRadius=").append(outerRadius);
        sb.append(", innerRadius=").append(innerRadius);
        sb.append(", endsClockwise=").append(endsClockwise);
        sb.append(", colours=").append(colours);
        sb.append(", endBuilder=").append(endBuilder);
        sb.append('}');
        return sb.toString();
    }
}
