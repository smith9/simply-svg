package svg

/**
 * User: david
 * Date: 13/07/13
 * Time: 9:39 PM
 */
class CartesianToSvgTransform implements Transform {
    Point transform = new Point(0, 0)
    BigDecimal scaleX = 1
    BigDecimal scaleY = -1

    CartesianToSvgTransform(Point transform, BigDecimal scaleX, BigDecimal scaleY) {
        this.transform = transform
        this.scaleX = scaleX
        this.scaleY = scaleY
    }

    CartesianToSvgTransform(Point transform) {
        this.transform = transform
    }

    Point transform(Point point) {
        return point.translate(transform).scale(scaleX, scaleY)
    }
}
