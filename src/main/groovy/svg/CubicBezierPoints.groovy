package svg

/**
 * User: david
 * Date: 12/07/13
 * Time: 5:20 PM
 */
public class CubicBezierPoints implements Shape {
    PointsList points = new PointsList()

    Point getStart() {
        return points[0]
    }

    CubicBezierPoints(Point start, Point controlStart, Point controlEnd, Point end) {
        points << start
        points << controlStart
        points << controlEnd
        points << end
    }
    CubicBezierPoints(List<Point> curvePoints) {
        points.addAll(curvePoints)
    }
    void addPoints(Point controlStart, Point controlEnd, Point end) {
        points << controlStart
        points << controlEnd
        points << end
    }

    void addPoints(CubicBezierPoints arc) {
        points.addAll(arc.points.tail())
    }

    Shape scale(BigDecimal scaleX, BigDecimal scaleY) {
        PointsList scaled = points.scale(scaleX, scaleY)
        return new CubicBezierPoints(scaled)
    }

    Shape translate(Point translate) {
        PointsList transformed = points.translate(translate)
        return new CubicBezierPoints(transformed)
    }

    Shape rotate(BigDecimal angle) {
        PointsList rotated = points.rotate(angle)
        return new CubicBezierPoints(rotated)
    }
    CubicBezierPoints reversePath() {
        return new CubicBezierPoints(points.reverse())
    }

    CubicBezierPoints mirrorOnX(BigDecimal x) {
        PointsList transformToOrigin = points.translate(new Point(-x as BigDecimal, 0))
        PointsList mirroredOnX = transformToOrigin.scale(-1, 1)
        PointsList result = mirroredOnX.translate(new Point(x, 0))
        return new CubicBezierPoints(result.reverse())
    }
    CubicBezierPoints mirrorOnY(BigDecimal y) {
        PointsList transformToOrigin = points.translate(new Point(0, -y as BigDecimal))
        PointsList mirroredOnY = transformToOrigin.scale(1, -1)
        PointsList result = mirroredOnY.translate(new Point(0, y))
        return new CubicBezierPoints(result)
    }


    @Override
    public java.lang.String toString() {
        final java.lang.StringBuilder sb = new java.lang.StringBuilder("CubicBezierPoints{");
        sb.append("points=").append(points);
        sb.append('}');
        return sb.toString();
    }
}
