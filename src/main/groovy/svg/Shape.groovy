package svg

/**
 * User: david
 * Date: 13/07/13
 * Time: 3:38 PM
 */
interface Shape {
    public Shape scale(BigDecimal scaleX, BigDecimal scaleY)
    public Shape translate(Point translate)
    public Shape rotate(BigDecimal angle)
}
