package svg

/**
 * User: david
 * Date: 13/07/13
 * Time: 10:06 AM
 */
class PointsList {
    @Delegate
    List<Point> points = []

    // this is not meant for high performance, consider using matrix transforms if performance was important
    // for my performance needs this is more readable applying transforms one by one as there are few points
    public PointsList rotate(BigDecimal angle) {
        operate { Point point ->
            point.rotate(angle)
        }
    }
    public PointsList scale(BigDecimal scaleX, BigDecimal scaleY) {
        operate { Point point ->
            point.scale(scaleX, scaleY)
        }
    }
    public PointsList translate(Point translation) {
        operate { Point point ->
            point.translate(translation)
        }
    }

    private PointsList operate(Closure operation) {
        return points.collect { Point point ->
            operation(point)
        } as PointsList
    }

    @Override
    public java.lang.String toString() {
        final java.lang.StringBuilder sb = new java.lang.StringBuilder("PointsList{");
        sb.append("points=").append(points);
        sb.append('}');
        return sb.toString();
    }
}
